import { configure } from '@kadira/storybook';

const loadStories = () => {
  let req = require.context('../components', true, /.stories.js$/);
  req.keys().forEach((filename) => req(filename));
};

configure(loadStories, module);
