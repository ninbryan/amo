import React from 'react';
import {storiesOf, action} from '@kadira/storybook';

storiesOf('Atoms', module)
  .add('An example of atoms', () => (
    <div>
      <a href="#" onClick={action('clicked')}>
        Hello anchor
      </a>
      
      <a href="#" onClick={action('clicked')}>
        <span>span test</span>
        <p>p test</p>
      </a>
      
      <button onClick={action('clicked')}>
        Hello button
      </button>
      
      <button onClick={action('clicked')}>
        <span>span test</span>
        <p>p test</p>
      </button>
    </div>
  ));
