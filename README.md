# amo

Atomic design. Modular components. Open source.

## Purpose

Exploring [atomic design](http://bradfrost.com/blog/post/atomic-web-design/) with [React](https://github.com/facebook/react) + [Storybooks](https://github.com/storybooks/react-storybook)

## TODO

- Build components with [jest tests](https://github.com/facebook/jest)
- Prepare stories
- Standard format
- create dist/ for umd & es6